/// <reference types="cypress" />

// Welcome to Cypress!
//
// This spec file contains a variety of sample tests
// for a todo list app that are designed to demonstrate
// the power of writing tests in Cypress.
//
// To learn more about how Cypress works and
// what makes it such an awesome testing tool,
// please read our getting started guide:
// https://on.cypress.io/introduction-to-cypress

describe('Test boton', () => {
  it('Clic 1', () => {
    cy.visit(Cypress.env('baseUrl'))
    cy.get('button').click()
    cy.wait(2000)  
  })
  it('Clic 2', () => {
    cy.visit(Cypress.env('baseUrl'))
    cy.get('button').click()
    cy.wait(2000)  
  })
  it('Clic 3', () => {
    cy.visit(Cypress.env('baseUrl'))
    cy.get('button').click()
  })    
})
