/// <reference types="cypress" />

// Welcome to Cypress!
//
// This spec file contains a variety of sample tests
// for a todo list app that are designed to demonstrate
// the power of writing tests in Cypress.
//
// To learn more about how Cypress works and
// what makes it such an awesome testing tool,
// please read our getting started guide:
// https://on.cypress.io/introduction-to-cypress

describe('Test logos', () => {
  it('Logo vite', () => {
    cy.visit(Cypress.env('baseUrl'))
    cy.get('[href="https://vitejs.dev"] > .logo').click()
    cy.wait(2000)  
  })
  it('Logo vue', () => {
    cy.visit(Cypress.env('baseUrl'))
    cy.get('[href="https://vuejs.org/"] > .logo').click()  
    cy.wait(2000)   
  })
})