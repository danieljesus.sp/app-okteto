import openai
import os
import sys
import subprocess

def get_last_two_commits():
    result = subprocess.run(['git', 'rev-list', '--max-count=2', 'HEAD'], stdout=subprocess.PIPE)
    revisions = result.stdout.decode('utf-8').split('\n')[:-1]
    return revisions

def get_modified_files(revisions):
    result = subprocess.run(['git', 'diff', '--name-only', revisions[1], revisions[0], '--', '*.py'], stdout=subprocess.PIPE)
    modified_files = result.stdout.decode('utf-8').split('\n')[:-1]
    return modified_files

def read_file(file_path):
    with open(file_path, 'r') as f:
        content = f.read()
    return content

def generate_code():
    revisions = get_last_two_commits()
    if len(revisions) < 2:
        print('No se encontraron suficientes revisiones para generar código')
        return
    modified_files = get_modified_files(revisions)
    if len(modified_files) == 0:
        print('No se encontraron archivos modificados')
        return
    code_to_generate = ''
    for file in modified_files:
        code_to_generate += read_file(file) + '\n'
    openai.api_key = os.environ['CHATGPT_API_KEY']
    response = openai.Completion.create(
        engine='davinci-codex',
        prompt=code_to_generate,
        max_tokens=50,
        n=1,
        stop=None,
        temperature=0.7,
    )
    generated_code = response.choices[0].text
    try:
        exec(generated_code)
        print("El código generado es válido")
    except Exception as e:
        print("El código generado no es válido")
        print(str(e))

if __name__ == '__main__':
    generate_code()
